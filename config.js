import fs from "fs";
import fetch from 'node-fetch';
import path from "path";

export default class fmi {
    #groups = {};
    #pathfile = null;
    
    constructor(struct) {
        for (let g in struct) {
            this.#groups[g] = new Group(struct[g]);
        }
    }
    connect(file,dir=process.cwd()){
        var exists = true;
        this.#pathfile = jpath(dir,file);
        if (!fs.existsSync(this.#pathfile) || !fs.lstatSync(this.#pathfile).isFile()) {
            console.warn("Config does't exists. It will be created: " + this.#pathfile);
            exists = false;
        }else{
            this.fromJUML(fs.readFileSync(this.#pathfile).toString());
        }
        this.save();
        return exists;
    }

    get(group, name, empty = null){
        if ((this.#groups[group] ?? {})[name] == null){
            return empty;
        }
        return this.#groups[group][name].valueOf();
    }
    set(group, name, value){
        if ((this.#groups[group] ?? {})[name] == null) {
            return false;
        }
        this.#groups[group][name].set(value);
        return true;
    }

    save(){
        if(this.#pathfile != null) fs.writeFileSync(this.#pathfile, this.toJUML());
    }

    fromJUML(juml) {
        var comment = [];
        let nowGroup = null;

        juml.split("\n").map(s => s.trim()).forEach(now => {
            if (now.startsWith("#")) {
                comment.push(now.slice(1));
            } else if (now.startsWith("[") && now.endsWith("]")) {
                let groupName = now.slice(1, -1);
                nowGroup = this.#groups[groupName] || null;
                nowGroup.setComment(comment.join("\n"));
                comment = [];
            } else if (now.includes("=") && nowGroup != null) {
                let nows = now.split("=");
                let name = nows.splice(0, 1)[0].trim();
                let data = nows.join("=").trim();
                if (nowGroup[name] != null){
                    nowGroup[name].set = data;
                    nowGroup[name].comment = comment.join("\n");
                    comment = [];
                }
            }
        });
    }
    toJUML() {
        let json = this.#groups;
        var out = "";
        for (let group in json) {
            if (json[group].comment() != "") {
                out += "\n";
                out += json[group].comment().split("\n").map(d => "#" + d).join("\n");
            }
            out += `\n[${group}]\n`;
            for (let name in json[group]) {
                if (json[group][name].comment != "") {
                    out += json[group][name].comment.split("\n").map(d => "#" + d).join("\n");
                    out += "\n";
                }
                out += name + "=" + json[group][name] + "\n";
            }
        }
        return out;
    }

    readPath (path,dir=process.cwd()){
        return new Promise((res,rej)=>{
            if (path.startsWith("http")) {
                fetch(path)
                .then(d=>d.text())
                .then(d=>res(d))
                .catch(()=>{
                    rej("Error occurred while fetching: " + path)
                })
            } else {
                const pathf = jpath(dir, path);
                if (!fs.existsSync(pathf) || !fs.statSync(pathf).isFile()) {
                    rej("Coud not find File: " + pathf);
                } else {
                    try {
                        var content = fs.readFileSync(pathf).toString();
                        res(content);
                    } catch (e) {
                        rej("Error occurred while reading: " + pathf);
                    }
                } 
            }
        });        
    }
    readPathes(...pathes) {
        return new Promise(async (res, rej) => {
            var out = [];
            for (var i = 0; i < pathes.length; i++) {
                try {
                    out[i] = await this.readPath(pathes[i]);
                } catch (e) {
                    rej(e);
                }
            }
            res(out);
        });
    };
}

class Group extends Object {
    #comment = "";
    constructor(struct) {
        super({});
        for (let e in struct) {
            let elem = struct[e];
            switch (elem.type || "string") {
                case "number": {
                    this[e] = new NField(elem.default ?? 0, elem.comment ?? "", elem.env ?? "");
                } break;
                case "boolean": {
                    this[e] = new BField(elem.default ?? false, elem.comment ?? "", elem.env ?? "");
                } break;
                case "string": {
                    this[e] = new SField(elem.default ?? "", elem.comment ?? "", elem.env ?? "");
                } break;
            }
        }
    }
    comment(){
        return this.#comment
    }
    setComment(c){
        this.#comment = c;
    }
}

class NField {
    comment = "";
    #value = 0;

    constructor(v, comment, env) {
        if (env != "" && process.env[env]) {
            this.#value = Number(createType(process.env[env]));
        } else {
            this.#value = Number(createType(v));
        }
        this.comment = comment;
    }
    /**
     * @param {Number} v
     */
    set set(v){
        this.#value = Number(createType(v));
    }
    valueOf() {
        return this.#value;
    }
}
class BField {
    comment = "";
    #value = false;

    constructor(v, comment, env) {
        if (env != "" && process.env[env]) {
            this.#value = Boolean(createType(process.env[env]));
        } else {
            this.#value = Boolean(createType(v));
        }
        this.comment = comment;
    }
    /**
     * @param {Boolean} v
     */
    set set(v) {
        this.#value = Boolean(createType(v));
    }
    valueOf() {
        return this.#value;
    }
}
class SField {
    comment = "";
    #value = "";

    constructor(v, comment, env) {
        if (env != "" && process.env[env]) {
            this.#value = String(createType(process.env[env]));
        } else {
            this.#value = String(createType(v));
        }
        this.comment = comment;
    }
    /**
     * @param {String} v
     */
    set set(v) {
        this.#value = String(createType(v));
    }
    valueOf() {
        return this.#value;
    }
}

function createType(data) {
    if (data == "true") {
        return true;
    }
    if (data == "false") {
        return false;
    }
    if (String(data).toLowerCase() == "infinity") {
        return Infinity;
    }
    if (String(data).toLowerCase() == "-infinity") {
        return -Infinity;
    }
    if (!isNaN(data)) {
        return Number(data);
    }
    return data;
}

function jpath(a, b) {
    if (b.startsWith("/")) {
        return b;
    }
    return path.join(a, b);
}