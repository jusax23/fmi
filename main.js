import fmi from "./config.js";
import path from "path";

const conf_struct = {
  System:{
    PORT:{type:"number",default:80},
    URL:{type:"string",default:"https://google.de/",comment:"The exposed URL:"}
  },
  ssl:{
    enable:{type:"boolean",default:true,env:"FMI_SSL_ENABELED"},
    privkey:{type:"string",default:"privkey.pem"},
    cert:{type:"string",default:"cert.pem"},
    chain:{type:"string",default:"chain.pem"}
  }
};

console.log()

const mc = new fmi(conf_struct);

mc.connect("./config.juml");

if(mc.get("ssl","enable")){
  console.log("SSL Enabled");
  mc.readPathes(mc.get("ssl","privkey"),mc.get("ssl","cert"),mc.get("ssl","chain"))
  .then(([privkey,cert,chain])=>{
    console.log("privkey: ",privkey,"\ncert: ",cert,"\nchain: ",chain);
  }).catch(err=>{
    console.error(err);
    process.exit();
  });
}else{
  console.log("SSL Disabled");
}